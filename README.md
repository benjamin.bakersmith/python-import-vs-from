# Python Import vs From behavior

This repo shows two examples of differing behavior between `import` and `from` for
referencing other modules.

## simple

Run `main.py` to see how incrementing the value of a variable in a shared module
can behave differently depending on whether other modules have imported that value
with `import` or `from` (by reference or copy).

    python ./simple/main.py

When the value of `shared.x` is incremented by `import_example` or `main` modules
(both of which use `import` statement for the `shared` module), they both see the
updated value, but `from_example` does not. Trying to increment `x` in the
`from_example` raises an exception because a local `x` variable is not defined.

## mocking

Run `pytest` to see how mocking external functions behaves differently for modules
that use `import` or `from`.

    pytest -vv ./mocking/

When the module under test uses `from` for an import, it has to be mocked within
the module under test itself, mocking the function within the `shared` module
directly will have no effect. But if the module under test uses `import`, then
the shared function can be mocked either in the source module or the module under
test.
