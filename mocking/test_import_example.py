import unittest.mock

import import_example


# mock the source method directly
def test_patch_source():
    with unittest.mock.patch("shared.always_2", return_value=9000):
        assert import_example.proxy_always_2() == 9000


# mock the source method relative to the module under test
def test_patch_module_under_test():
    with unittest.mock.patch("import_example.shared.always_2", return_value=9000):
        assert import_example.proxy_always_2() == 9000
