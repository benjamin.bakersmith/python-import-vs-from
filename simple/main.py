import from_example
import import_example
import shared


def main_increment_x():
    shared.x += 1


def main_print_x():
    print(f"main:           {shared.x}")


def main():
    print("\ninitial state\n-----------------")
    main_print_x()
    import_example.print_x()
    from_example.print_x()

    print("\nincrement shared.x in main\n-----------------")
    main_increment_x()

    main_print_x()
    import_example.print_x()
    from_example.print_x()

    print("\nincrement shared.x in import_example\n-----------------")
    import_example.increment_x()

    main_print_x()
    import_example.print_x()
    from_example.print_x()

    print("\nincrement x in from_example\n-----------------")
    from_example.increment_x()


if __name__ == "__main__":
    main()
