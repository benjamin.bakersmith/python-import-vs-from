from shared import x


def increment_x():
    try:
        # this will raise an error because x isn't a local variable  # noqa
        x += 1
    except Exception as e:
        print(e)


def print_x():
    print(f"from_example:   {x}")
